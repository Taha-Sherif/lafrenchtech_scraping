from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import time
import csv


def browse(url):

    #getting chromedriver path
    script_dir = Path(__file__).parent
    path=(script_dir / 'chromedriver').resolve()
    #disabling images
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    #import chromedriver
    driver = webdriver.Chrome(path,chrome_options=chrome_options)

    # getting the website
    driver.get(url)
    driver.implicitly_wait(5)
    return driver 

def connect(driver,email,psw):
    button = driver.find_element_by_xpath('//*[@id="app"]/div/div[1]/header/div[2]/div/button[2]')
    ActionChains(driver).move_to_element(button).click().perform()
    driver.find_element_by_name('email').send_keys(email)
    driver.find_element_by_name('password').send_keys(psw)
    login = driver.find_element_by_id('login')
    ActionChains(driver).move_to_element(login).click().perform()
    time.sleep(5)

def write_companies_info_in_file(driver, companies, file_name):
    with open(file_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        for elem in companies :
            wr.writerow(elem)
    fp.close()

def main ():
    driver = browse('https://ecosystem.lafrenchtech.com/')
    connect(driver,'taha.sherif@breakpoint-technology.fr','123tahaaSHEERIF&')
    
    driver.implicitly_wait(5)
    accelerators = driver.find_element_by_id('accelerators-menu-item')
    ActionChains(driver).move_to_element(accelerators).click().perform()
    driver.implicitly_wait(10)

    accelerators = []
    while True :
        for j in range (1,25):

            accelerator_info = []
            #name                
            try:
                accelerator_name = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[1]/div/div/div[2]/div[1]/a').text
                accelerator_info.append(accelerator_name)
            except:
                break
            #prominence rank               
            try:
                accelerator_prominence_rank = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[1]/div').text
                accelerator_info.append(accelerator_prominence_rank)
            except:
                accelerator_info.append('')

            #number of round            
            try:
                accelerator_round = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[2]').text
                accelerator_info.append(accelerator_round)
            except:
                accelerator_info.append('')

            #location          
            try:
                accelerator_location = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[3]').text
                accelerator_info.append(accelerator_location)
            except:
                accelerator_info.append('')
            
            #top investments
            try:
                accelerator_location = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[4]').text
                accelerator_info.append(accelerator_location)
            except:
                accelerator_info.append('')
            
            print(accelerator_info)
            accelerators.append(accelerator_info)
        try:
            show_more = driver.find_element_by_xpath('//*[@id="next-page"]').get_attribute('href')
            driver.get(show_more)
            driver.implicitly_wait(5)
            time.sleep(3)
        except:
            break

    print(str(len(accelerators)) + 'accelerators')
    print('finish !')
    write_companies_info_in_file(driver,accelerators,'accelerators.csv')
    print('Save done succesfully')
    time.sleep(3)
    driver.quit()

main()
            