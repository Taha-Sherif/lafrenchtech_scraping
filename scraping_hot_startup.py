from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import time
import csv


def browse(url):

    #getting chromedriver path
    script_dir = Path(__file__).parent
    path=(script_dir / 'chromedriver').resolve()
    #disabling images
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    #import chromedriver
    driver = webdriver.Chrome(path,chrome_options=chrome_options)

    # getting the website
    driver.get(url)
    driver.implicitly_wait(10)
    time.sleep(3)
    return driver 

def connect(driver,email,psw):
    button = driver.find_element_by_xpath('//*[@id="app"]/div/div[1]/header/div[2]/div/button[2]')
    ActionChains(driver).move_to_element(button).click().perform()
    driver.find_element_by_name('email').send_keys(email)
    driver.find_element_by_name('password').send_keys(psw)
    login = driver.find_element_by_id('login')
    ActionChains(driver).move_to_element(login).click().perform()
    time.sleep(5)

def write_companies_info_in_file(driver, companies, file_name):
    with open(file_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        for elem in companies :
            wr.writerow(elem)
    fp.close()

def main ():
    driver = browse('https://ecosystem.lafrenchtech.com/')
    connect(driver,'taha.sherif@breakpoint-technology.fr','123tahaaSHEERIF&')
    
    driver.implicitly_wait(5)
    funding_rounds = driver.find_element_by_id('dashboard-menu-item')
    ActionChains(driver).move_to_element(funding_rounds).click().perform()
    driver.implicitly_wait(10)

    driver.get('https://ecosystem.lafrenchtech.com/companies.startups/f/company_status/not_acquired/employee_12_months_growth_rank/anyof_top%20half_top%2025%25_top%2010%25_top%205%25/employees/anyof_2-10_11-50/launch_year_min/anyof_2010/locations/allof_France?selectedColumns=name%2ClaunchDate%2ChqLocations&sort=-employee_12_months_growth_delta')
    driver.implicitly_wait(10)
    time.sleep(3)
    hot_startups = []

    while True :
        for j in range (1,25):
            hot_startup_info = []
            # name                
            try:
                hot_startup_name = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[1]/div/div/div[2]/div[1]/a').text
                hot_startup_info.append(hot_startup_name)
            except:
                break

            #launch date               
            try:                          
                launch_date  = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[1]').text
                hot_startup_info.append(launch_date)
            except:
                hot_startup_info.append('')

            #location               
            try:
                location  = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j)  +']/div[2]/div[2]').text
                hot_startup_info.append(location)
            except:
                hot_startup_info.append('')

            print(hot_startup_info)
            hot_startups.append(hot_startup_info)
        try:
            show_more = driver.find_element_by_xpath('//*[@id="next-page"]').get_attribute('href')
            driver.get(show_more)
            driver.implicitly_wait(5)
            time.sleep(3)
        except:
            break

    print(str(len(hot_startups)) + ' hot startups')
    write_companies_info_in_file(driver,hot_startups,'hot_startups.csv')
    print('Save done succesfully')
    time.sleep(3)
    driver.quit()

main()
