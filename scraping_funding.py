from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import time
import csv


def browse(url):

    #getting chromedriver path
    script_dir = Path(__file__).parent
    path=(script_dir / 'chromedriver').resolve()
    #disabling images
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    #import chromedriver
    driver = webdriver.Chrome(path,chrome_options=chrome_options)

    # getting the website
    driver.get(url)
    driver.implicitly_wait(5)
    return driver 

def connect(driver,email,psw):
    button = driver.find_element_by_xpath('//*[@id="app"]/div/div[1]/header/div[2]/div/button[2]')
    ActionChains(driver).move_to_element(button).click().perform()
    driver.find_element_by_name('email').send_keys(email)
    driver.find_element_by_name('password').send_keys(psw)
    login = driver.find_element_by_id('login')
    ActionChains(driver).move_to_element(login).click().perform()
    time.sleep(5)

def write_companies_info_in_file(driver, companies, file_name):
    with open(file_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        for elem in companies :
            wr.writerow(elem)
    fp.close()

def main ():
    driver = browse('https://ecosystem.lafrenchtech.com/')
    connect(driver,'taha.sherif@breakpoint-technology.fr','123tahaaSHEERIF&')
    
    driver.implicitly_wait(5)
    funding_rounds = driver.find_element_by_id('funding-rounds-menu-item')
    ActionChains(driver).move_to_element(funding_rounds).click().perform()
    driver.implicitly_wait(10)

    driver.get('https://ecosystem.lafrenchtech.com/transactions.rounds/f/locations/allof_France/rounds/anyof_GRANT_SEED_EARLY%20VC')
    funding_rounds = []
    loop = True
    while loop :
        for j in range (1,25):
            funding_round_info = []
            # name                
            try:
                funding_round_name = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j) +']/div[1]/div/div/div[2]/div[1]/a').text
                funding_round_info.append(funding_round_name)
            except:
                break

            #investors               
            try:
                investors  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j) +']/div[2]/div[1]').text
                funding_round_info.append(investors)
            except:
                funding_round_info.append('')

            #market               
            try:
                market  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j)  +']/div[2]/div[2]').text
                funding_round_info.append(market)
            except:
                funding_round_info.append('')

            #location               
            try:
                location  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j)  +']/div[2]/div[3]').text
                funding_round_info.append(location)
            except:
                funding_round_info.append('')

            #valuation               
            try:
                valuation  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j)  +']/div[2]/div[4]').text
                funding_round_info.append(valuation)
            except:
                funding_round_info.append('')           

            #last round               
            try:
                last_round  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j)  +']/div[2]/div[5]').text
                funding_round_info.append(last_round)
            except:
                funding_round_info.append('')

            #date            
            try:
                date  = driver.find_element_by_xpath('//*[@id="window-scrollbar"]/div[1]/main/div/div/div/div/div[2]/div[2]/div/div[' +str(j)  +']/div[2]/div[6]').text
                funding_round_info.append(date)
            except:
                funding_round_info.append('')

            print(funding_round_info)
            funding_rounds.append(funding_round_info)
            
        try:
            show_more = driver.find_element_by_xpath('//*[@id="next-page"]').get_attribute('href')
            driver.get(show_more)
            driver.implicitly_wait(5)
            time.sleep(3)
        except:
            break
    
    print(str(len(funding_rounds)) + ' funding rounds')
    write_companies_info_in_file(driver,funding_rounds,'funding_rounds.csv')
    print('Save done succesfully')
    time.sleep(3)
    driver.quit()

main()