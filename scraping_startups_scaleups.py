from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import time
import csv


def browse(url):

    #getting chromedriver path
    script_dir = Path(__file__).parent
    path=(script_dir / 'chromedriver').resolve()
    #disabling images
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    #import chromedriver
    driver = webdriver.Chrome(path,chrome_options=chrome_options)

    # getting the website
    driver.get(url)
    driver.implicitly_wait(5)
    return driver 

def connect(driver,email,psw):
    button = driver.find_element_by_xpath('//*[@id="app"]/div/div[1]/header/div[2]/div/button[2]')
    ActionChains(driver).move_to_element(button).click().perform()
    driver.find_element_by_name('email').send_keys(email)
    driver.find_element_by_name('password').send_keys(psw)
    login = driver.find_element_by_id('login')
    ActionChains(driver).move_to_element(login).click().perform()
    time.sleep(5)

def write_companies_info_in_file(driver, companies, file_name):
    with open(file_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        for elem in companies :
            wr.writerow(elem)
    fp.close()

def main ():
    driver = browse('https://ecosystem.lafrenchtech.com/')
    connect(driver,'taha.sherif@breakpoint-technology.fr','123tahaaSHEERIF&')
    
    driver.implicitly_wait(5)
    startups_scaleups = driver.find_element_by_id('startups-scaleups-menu-item')
    ActionChains(driver).move_to_element(startups_scaleups).click().perform()
    driver.implicitly_wait(10)

    driver.get('https://ecosystem.lafrenchtech.com/companies.startups/f/locations/allof_France?row_index=10024')
    companies = []
    pages_nb = 0
    while True:
        for j in range (1,25):
            company_info = []
            #company name                
            try:
                company_name = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[1]/div/div/div[2]/div[1]/a').text
                company_info.append(company_name)
            except:
                break

            #company market
            try:
                company_market = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[1]/div').text
                company_info.append(company_market)
            except:
                company_info.append('')
                
            #company type
            try:
                company_location = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[2]/div').text
                company_info.append(company_location)
            except:
                company_info.append('')

            #company employees
            try:
                company_employees = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[3]/div/div[1]/span').text
                company_info.append(company_employees)
            except:
                company_info.append('')   

            #company launch date
            try:
                company_date = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[4]').text
                company_info.append(company_date)
            except:
                company_info.append('')

            #show the 5 next columns  
            for i in range (5):
                next_col = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[1]/div/button[1]')
                ActionChains(driver).move_to_element(next_col).click().perform()
                driver.implicitly_wait(5)

            # company funding
            try:
                company_funding = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[1]').text
                company_info.append(company_funding)
            except:
                company_info.append('') 
                
            # company location
            try:
                company_location = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[2]').text
                company_info.append(company_location)
            except:
                company_info.append('')

            # company last round
            try:
                company_last_round = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[3]').text
                company_info.append(company_last_round)
            except:
                company_info.append('')

            #show the 5 next columns  
            for i in range (5):
                next_col = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[1]/div/button[1]')
                ActionChains(driver).move_to_element(next_col).click().perform()
                driver.implicitly_wait(5) 

            # company revenue
            try:
                company_revenue = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[1]').text
                company_info.append(company_revenue)
            except:
                company_info.append('')

            # company growth stage
            try:
                company_growth_stage = driver.find_element_by_xpath('//*[@id="list-map-list"]/div/div[2]/div/div[' +str(j) +']/div[2]/div[3]').text
                company_info.append(company_growth_stage)
            except:
                company_info.append('')

            companies.append(company_info)
            print(company_info)

            driver.refresh()
            driver.implicitly_wait(10)

        try:
            show_more = driver.find_element_by_xpath('//*[@id="next-page"]').get_attribute('href')
            driver.get(show_more)
            driver.implicitly_wait(5)
            time.sleep(3)
            pages_nb += 1
            i += 1
        except:
            break
    
    print(str(len(companies)) + ' companies')
    print(str(pages_nb) + ' page')
    write_companies_info_in_file(driver,companies,'startup_scaleups_suite.csv')
    print('Save done succesfully')
    time.sleep(3)
    driver.quit()

main()
